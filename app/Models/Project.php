<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'projects';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'description'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 *
	 */
	public function tasks()
	{
		return $this->hasMany('Task');
	}

	/**
	 *
	 */
	public function users()
	{
		return $this->belongsToMany('App\Models\User', 'users_projects','project_id', 'user_id' );
	}
}
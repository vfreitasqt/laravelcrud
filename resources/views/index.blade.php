@extends('template')

@section('content')
	<div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Priority</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->name }}}}</td>
                            <td>{{ $task->priority }}</td>
                        </tr>
                    @endforeach
                </tbody>

        </div>
    </div>
@endsection
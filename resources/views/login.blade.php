
@extends('template')

@section('content')

	<div class="row">
        <div class="col-md-12">
        	<form action="/login" method="POST">
				{!! csrf_field() !!}
				<label>E-mail</label>
				<input type="email" name="email" value="{{ old('email')  }}" id="email" placeholder="example@email.com"/>

				<label>Password</label>
				<input type="password" name="password" id="pass" placeholder="Password"/>

				<div>
					<input type="checkbox" name="remember" id="remember"/>
					<span>Remember me | <a href="">Forgot Password</a></span>
				</div>

				<input type="submit" value="Sign in"/>
			</form>
        </div>
    </div>

	

@endsection
<!DOCTYPE html>
<html>
<head>
	<title>Laravel CRUD App</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
	<div class="container">

		<div class="navbar">


			<div class="navbar-header">
				<a class="navbar-brand" href="/">
					<img src="http://vignette4.wikia.nocookie.net/ideas/images/4/4d/TESTOnline-logo.gif/revision/latest?cb=20131204073919" style="height: 50px;" alt="Onsitez Logo"/>
				</a>

			</div>
		</div>

        <div style="height: 200px;"></div>

		@yield('content')
	</div>
</body>
</html>
<?php

use Illuminate\Database\Seeder;
use App\Models\Project;
use App\Models\User;

class TestModelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project = Project::create([
			'name'			=> 'Projeto 1',
			'description'	=> 'A project to do something...'
		]);

		$user = User::create([
			'name'		=> 'Vitor',
			'email'		=> 'vitor2@quicktech.no',
			'password' 	=> bcrypt('12345')
		]);


		$this->command->info($project->id);

		$project->users()->attach($user->id);
    }
}
